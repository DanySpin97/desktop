# Copyright 2012 NAKAMURA Yoshitaka
# Distributed under the terms of the GNU General Public License v2

SCM_SECONDARY_REPOSITORIES="
    breakpad
    gtest
    gyp
    japanese_usage_dictionary
    jsoncpp
    protobuf
    zinnia
    benchmark
"
SCM_benchmark_REPOSITORY="https://github.com/google/benchmark.git"
SCM_breakpad_REPOSITORY="https://chromium.googlesource.com/breakpad/breakpad"
SCM_fontTools_REPOSITORY="https://github.com/googlei18n/fonttools.git"
SCM_gtest_REPOSITORY="https://github.com/google/googletest.git"
SCM_gyp_REPOSITORY="https://chromium.googlesource.com/external/gyp"
SCM_japanese_usage_dictionary_REPOSITORY="https://github.com/hiroyuki-komatsu/japanese-usage-dictionary.git"
SCM_jsoncpp_REPOSITORY="https://github.com/open-source-parsers/jsoncpp.git"
SCM_protobuf_REPOSITORY="https://github.com/google/protobuf.git"
SCM_zinnia_REPOSITORY="https://github.com/taku910/zinnia.git"
SCM_EXTERNAL_REFS="
    src/third_party/breakpad:breakpad
    src/third_party/gtest:gtest
    src/third_party/gyp:gyp
    src/third_party/japanese_usage_dictionary:japanese_usage_dictionary
    src/third_party/jsoncpp:jsoncpp
    src/third_party/protobuf:protobuf
    src/third_party/zinnia:zinnia
"
SCM_protobuf_EXTERNAL_REFS="
    third_party/benchmark:benchmark
"

SCM_REPOSITORY="https://github.com/google/mozc.git"
SCM_REVISION="afb03ddfe72dde4cf2409863a3bfea160f7a66d8"

require scm-git

require elisp [ with_opt=true source_directory=unix/emacs ] python [ blacklist=3 has_bin=false has_lib=false multibuild=false ]

SUMMARY="Japanese Input Method"
HOMEPAGE="https://github.com/google/mozc"
DOWNLOADS=""

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="ibus
    ( providers: qt5 ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/clang[>=3.4]
        dev-lang/python:2.7
        sys-devel/ninja
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        x11-libs/libxcb
        ibus? ( inputmethods/ibus[>=1.4.1] )
        providers:qt5? ( x11-libs/qtbase:5 )
"

WORK="${WORK}/src"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p2 "${FILES}"/gcc8.patch
)

src_prepare() {
    default

    edo sed -e '/enable_gtk_renderer/s/1/0/' -i build_mozc.py

    # mozc hardcod some tools
    local dir=${WORKBASE}/symlinked-build-tools
    edo mkdir -p ${dir}
    for t in ar ld nm pkg-config readelf; do
        edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-${t} ${dir}/${t}
    done
    edo ln -s /usr/$(exhost --target)/bin/python2 ${dir}/python
    export PATH="${dir}:${PATH}"
}

src_configure() {
    # mozc uses $CXX as a linker by default
    # but if $LD is defined, use it instead and fail to link
    unset LD

    GYP_DEFINES="ibus_mozc_path=/usr/$(exhost --target)/lib/ibus-mozc/ibus-engine-mozc enable_gtk_renderer=0 python_executable=python2" \
        edo python2 build_mozc.py gyp --server_dir=/usr/$(exhost --target)/lib/mozc $(option !providers:qt5 --noqt)
}

src_compile() {
    local myargs=(
        server/server.gyp:mozc_server
    )

    if option providers:qt5; then
        myargs+=(
            gui/gui.gyp:mozc_tool
        )
    fi

    if option emacs; then
        myargs+=(
            unix/emacs/emacs.gyp:mozc_emacs_helper
        )
    fi

    if option ibus; then
        myargs+=(
            unix/ibus/ibus.gyp:ibus_mozc
        )
    fi

    edo python2 build_mozc.py build -c Release "${myargs[@]}"

    elisp_src_compile
}

# ibus-mozc test hangs
RESTRICT="test"

src_test() {
    esandbox allow_net --bind unix-abstract:tmp/.mozc.*
    esandbox allow_net --connect unix-abstract:tmp/.mozc.*
    esandbox allow_net unix-abstract:tmp/.mozc.*
    esandbox allow_net --connect inet:127.0.0.1@6000
    esandbox allow_net inet:127.0.0.1@6000
    edo python2 build_mozc.py runtests -c Release
    esandbox disallow_net --bind unix-abstract:tmp/.mozc.*
    esandbox disallow_net --connect unix-abstract:tmp/.mozc.*
    esandbox disallow_net unix-abstract:tmp/.mozc.*
    esandbox disallow_net --connect inet:127.0.0.1@6000
    esandbox disallow_net inet:127.0.0.1@6000
}

src_install() {
    exeinto /usr/$(exhost --target)/lib/mozc
    doexe out_linux/Release/mozc_server
    if option providers:qt5; then
        doexe out_linux/Release/mozc_tool
    fi

    elisp_src_install

    if option emacs; then
        dobin out_linux/Release/mozc_emacs_helper
    fi

    if option ibus; then
        exeinto /usr/$(exhost --target)/lib/ibus-mozc/
        newexe out_linux/Release/ibus_mozc ibus-engine-mozc

        insinto /usr/share/ibus/component
        doins out_linux/Release/gen/unix/ibus/mozc.xml

        edo pushd data/images/unix
        insinto /usr/share/ibus-mozc
        newins ime_product_icon_opensource-32.png product_icon.png
        for f in ui-*.png; do
            newins ${f} ${f#ui-}
        done
        edo popd
    fi
}

